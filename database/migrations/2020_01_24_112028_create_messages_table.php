<?php
/**
 * we created withe the message Modle by this code ({php artisan make:model Message -m})
 * here we add the table info (coloms and it types)
 */
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /**
     * this fun is for creat the table with the coloms we want
     * this coloms are (id=int incremynt, name=string, email=string, message=mediumText{a long text})
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email');
            $table->mediumText('message');
            $table->timestamps();
        });
        /**
         * then we go to .env and enter the DB info 
        */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
