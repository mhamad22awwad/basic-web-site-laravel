<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * route to go to the home page
 */
// Route::get('/', function () {
//     return view('home');
// });
////////////////////////
/**
 * the best to do is this
 * 1) creat a Controler for the Pages withe): ( php artisan make:Controller PagesController )
 * 2) move the ( return view('home'); ) to the file and call it from here like this
 * (( Route::get('/home' , 'PagesController@getHome'); ))
 * getHome is the fun name in the PagesController
 * '/home' is the link we will viset to see the page
 */
Route::get('/' , 'PagesController@getHome');

/**
 * route to go to the welcome page
 */
// Route::get('/welcome', function () {
// return view('welcome');
// });
Route::get('/welcome' , 'PagesController@getWelcome');

/**
 * route to go to the links page
 */
// Route::get('/links' , function () {
// return view('links');
// });
Route::get('/links' , 'PagesController@getLinks');

/**
 * route to go to the about page
 */
// Route::get('/about', function () {
//     return view('about');
// });
Route::get('/about' , 'PagesController@getAbout');

/**
 * route to go to the contact page
 */
// Route::get('/contact', function () {
// return view('contact');
// });
Route::get('/contact' , 'PagesController@getContact');

/**
 * this route(link) is to send data for the file (app/Http/Controllers/MessageController.php) to th function (submit)
 * we creat the MessageController with the code::  php artisan make:controller MessageController
 */
Route::post('/contact/submit' , 'MessageController@submit');

/**
 * this route(link) is to get all the data in the DB & show them in a page
 */
Route::get('/messages' , 'MessageController@getMessage');
