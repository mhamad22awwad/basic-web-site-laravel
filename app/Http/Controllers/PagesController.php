<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

/**
 * in here we put all the view for the pages we will create
 * and then call them in the web.php page
 */
class PagesController extends Controller {

    /** 
     * this well display the home page when the fun is called like
     * (( PagesController@getHome ))
     */ 
    public function getHome(){
        return view('home');
    }

    public function getWelcome(){
        return view('welcome');
    }

    public function getLinks(){
        return view('links');
    }

    public function getAbout(){
        return view('about');
    }

    public function getContact(){
        return view('contact');
    }



}
