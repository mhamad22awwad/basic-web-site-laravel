<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// we conect the Message model so we can insert data to DB
use App\Message;

/**
 * in here we will wrthe the functions code in here 
 * we writh the submit fun in here
 */
class MessageController extends Controller {

    /**
     * the fun is for the contact form (contact/submit)
     * he will get all the data from the form 
     * and will insert theme in the DB
     */
    public function submit(Request $request){
        
        /**
         * this will return the data the data in the form in email
         * and will by printed
         */
        // return $request->input("email");
        
        /**
         * this will chick in the form for the name & email 
         * and chick if thay are empty or not empty
         * if there any data in name & email well return SUCCESS
         * if there no any data it will stay in the "/contact" link
         * and dont send or do any thenk
         */
        $this->validate($request , [
            'name'  => 'required',
            'email' => 'required'
        ]);
        // return a message if we have data in the form and we get them
        // return "SUCCESS";

        // creat a new message Opject and insert the data in the DB
        $message = new Message;

        // get the name and insert it in the name colome
        $message->name    = $request->input('name');
        // get the email and insert it in the emai colome
        $message->email   = $request->input('email');
        // get the message and insert it in the message colome
        $message->message = $request->input('message');

        // the quere(the procces of save the data we insert them in the DB)
        $message->save();

        // go to this link(route)
        // return redirect('/home');

        // go to home link and
        // and send (session) that have data (Message Sent, Thanks)
        return redirect('/')->with('success' , 'Message Sent, Thanks');

    }

    /**
     * this fun well select (get) all the data in the message table in DB
     */
    public function getMessage(){
        // get all the messeag and set them in this var ($messages)
        $messages = Message::all();

        // go to messages View with all the data in the messages
        return view('messages')->with('messages' , $messages) ;
    }

    


}
