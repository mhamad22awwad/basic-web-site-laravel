{{-- 
    used to get all the code from layouts.app ((all the html & css code))
    so we dont keep repit the code every time in all the files    
--}}

{{-- the general page we well get the struct of the <html> --}}
@extends('layouts.app')

{{-- set the title  --}}
@section('title', '| Links')
{{-- 
    here is where all the code we put them in ((content))
    so it show in the (content section) theat are in the layouts.app     
--}}
@section('content')
    
    <h1><a href="http://basicwebsite.dev/welcome"> welcome page </a></h1>
    <h1><a href="http://basicwebsite.dev/">home page</a></h1>
    <h1><a href="http://basicwebsite.dev/about">about page</a></h1>
    <h1><a href="http://basicwebsite.dev/contact">contact page</a></h1>
    <h1><a href="http://basicwebsite.dev/messages">messages page</a></h1>

@endsection