{{-- <div class="jumbotron text-white text-center rounded bg-dark display-4 ">
</div> --}}


<div class="jumbotron font-italic">
    {{-- 
        this showcase will be show in the home page only in like this
        white the if in the showcase.blade.php 
            @if(Request::is('home'))
                @include('inc.showcase')
            @endif    
    --}}
    <div class="container">
      <h1 class="display-3 text-center">Hello, world!</h1>
      <h2 class="display-4"> Welcome to Basic Web Site Laravel</h2>
      <p>This is a template for a simple marketing or informational website. 
          It includes a large callout called a jumbotron and three supporting pieces of content. 
          Use it as a starting point to create something more unique.</p>
    </div>
  </div>