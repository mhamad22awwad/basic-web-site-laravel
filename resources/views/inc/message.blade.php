{{-- 
    in here the if will check if there any error returnd here (no name or email)
    and it will display a error message 
--}}
@if (count($errors) > 0)
    {{-- for loop for errors arr --}}
    @foreach ($errors->all() as $error)
        <div class="">
            <div class="alert alert-danger">
                {{-- the text message that will dislay --}}
                {{$error}}
            </div>
        </div>
    @endforeach
@endif

{{-- this alert for the user till heme that the message have sent --}}
@if (session('success'))
    <div class="alert alert-success">
        {{session('success')}}
    </div>
@endif