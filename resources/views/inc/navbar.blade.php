<nav class="navbar navbar-expand-md mb-4 navbar-dark bg-dark">
  <a class="navbar-brand" href="/">Welcome Laravel</a>
  <div>
    <ul class="navbar-nav mr-auto">
      <li class="nav-item {{-- chike the route to active the nav link --}} {{Request::is('/') ? 'active' : ''}} ">
        <a class="nav-link" href="/">Home</a>
      </li>
      <li class="nav-item {{-- chike the route to active the nav link --}} {{Request::is('links') ? 'active' : ''}} ">
        <a class="nav-link" href="/links">Links</a>
      </li>
      <li class="nav-item {{-- chike the route to active the nav link --}} {{Request::is('about') ? 'active' : ''}} ">
        <a class="nav-link" href="/about">About</a>
      </li>
      <li class="nav-item {{-- chike the route to active the nav link --}} {{Request::is('contact') ? 'active' : ''}} ">
        <a class="nav-link" href="/contact">Contact</a>
      </li>
      <li class="nav-item {{-- chike the route to active the nav link --}} {{Request::is('messages') ? 'active' : ''}} ">
        <a class="nav-link" href="/messages">Messages</a>
      </li>      
    </ul>
  </div>
</nav>