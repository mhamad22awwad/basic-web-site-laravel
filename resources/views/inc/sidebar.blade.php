{{-- 
    the sidebar will be show in all the pages witch have {{@extends('layouts.app')}}
    and thay all will have it the same place
    
    ## in the home page i have make some new {{@section('sidebar')}}
    ## but it stil show the old {{@section('sidebar')}} white the new
--}}
@section('sidebar')
<!-- sidebar in all pages -->
<div>
    <div class="card">
        <div class="card-body">
            <h3> this is a SideBar</h3>
            <p> this sidebar will be show in all pages </p>
            {{-- <p> {{Route::get('sidebar' , 'MessageController@getNumMessage');}} </p> --}}
            {{-- {{ $data }} --}}
        </div>

        @show    
    </div>

</div>