{{-- the general page we well get the struct of the <html> --}}
@extends('layouts.app')

{{-- set the title  --}}
@section('title', '| Contact')

@section('content')
{{-- this code used to enabil the form in the webSite --}}
{{-- composer require laravelcollective/html --}}


    <h1> Content </h1>

    <div>

        {!! Form::open(['url' => 'contact/submit']) !!}

            <div class="form-group">
                {{ Form::label('name', 'The Name', ['class' => 'some-class'])}}
                {{-- {{ Form::[/type==/]text([/name,id  ==/]'email',[/value ==/] 'example@gmail.com') , [/att for the input/] $attributes = ['placeholder' => 'Enter The Email' ] }} --}}
                {{ Form::text('name', null, ['placeholder' => 'Enter Your Name' , 'class' => 'form-control' ]) }}
            </div>

            <div class="form-group">
                {{ Form::label('email', 'E-Mail Address', ['class' => 'some-class'])}}
                {{-- {{ Form::text('email', 'example@gmail.com') }} --}}
                {{ Form::email('email', $value = null, $attributes = ['placeholder' => 'Enter The Email' , 'class' => 'form-control' ]) }}
            </div>

            
            <div class="form-group">
                {{ Form::label('message', 'The Message', ['class' => 'some-class'])}}
                {{-- {{ Form::text('email', 'example@gmail.com') }} --}}
                {{ Form::textarea('message', $value = null, $attributes = ['placeholder' => 'Enter The Message' , 'class' => 'form-control' ]) }}
            </div>

            {{ Form::submit('Click Me!' , $attributes = ['class' => 'btn btn-primary' ]) }}

        {!! Form::close() !!}

    </div>

@endsection
