{{-- the general page we well get the struct of the <html> --}}
@extends('layouts.app')

@section('content')

    <h1> Home </h1>
    

    <div>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
            Dolorum impedit laboriosam iusto eius sit porro rem quibusdam sapiente 
            ad a magni ea ipsam, asperiores in fugit quisquam tenetur obcaecati est.</p>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
            Dolorum impedit laboriosam iusto eius sit porro rem quibusdam sapiente 
            ad a magni ea ipsam, asperiores in fugit quisquam tenetur obcaecati est.</p>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
            Dolorum impedit laboriosam iusto eius sit porro rem quibusdam sapiente 
            ad a magni ea ipsam, asperiores in fugit quisquam tenetur obcaecati est.</p>

    </div>

@endsection

{{-- 
    this is a new {{@section('sidebar')}}
    and it only show in here
    and it show the old one 
    because it have the {{ @parent == the old code we have alredy with the new code}}
--}}
@section('sidebar')
    
    {{-- it will show the old sidebar --}}
    @parent
    
    {{-- it will show in the sidebar in  home only --}}
    <div class="card-body">
        <h1>this sidebar is only in home</h1>
    </div>
@endsection