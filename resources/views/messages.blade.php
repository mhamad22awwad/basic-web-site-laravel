{{-- the general page we well get the struct of the <html> --}}
@extends('layouts.app')

{{-- set the title  --}}
@section('title', '| Messages')

@section('content')

    @if (count($messages))
    
        @foreach ($messages as $message)
        
            <ul class="list-group">
                <li class="list-group-item list-group-item-action h2">Message Number {{$message->id}} From : {{$message->name }}</li>
                <li class="list-group-item list-group-item-action h2">Email : {{$message->email }}</li>
                <li class="list-group-item list-group-item-action h3">The Message is : {{$message->message }}</li>
            </ul>
            <br>
        @endforeach
    @endif

    

    
@endsection


@section('sidebar')
    
    {{-- it will show the old sidebar --}}
    @parent

@endsection