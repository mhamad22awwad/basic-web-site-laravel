{{-- the general page we well get the struct of the <html> --}}
@extends('layouts.app')

{{-- set the title  --}}
@section('title', '| About')

{{-- the contact of the page --}}
@section('content')

    <h1> About </h1>
    <div>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
            Dolorum impedit laboriosam iusto eius sit porro rem quibusdam sapiente 
            ad a magni ea ipsam, asperiores in fugit quisquam tenetur obcaecati est.</p>
        <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. 
            Dolorum impedit laboriosam iusto eius sit porro rem quibusdam sapiente 
            ad a magni ea ipsam, asperiores in fugit quisquam tenetur obcaecati est.</p>
    </div>
@endsection