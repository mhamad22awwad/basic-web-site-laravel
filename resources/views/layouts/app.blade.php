<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title> 
        Basic Web Site 
        {{-- here is the dynamic titel well change as the page we in --}} 
        {{-- here is the general page for all the pages --}}
        @yield('title')
    </title>

    {{-- we link the css {public/css/app.css} code & scss  --}}
    <link rel="stylesheet" href="/css/app.css">
</head>

<body>

    {{-- it will display the navbar file in top of all the pages --}}
    @include('inc.navbar')

    {{-- 
        here where all the code will be puted 
        (all the code are inn the {@section('content') --to--> @endsection}  )  
        all the code are in the other files like {home.blade.php // about.blade.php} 
        
        ## it will display the showcase in the home page only 
    --}}
    <div class="container">
        
        {{-- 
            it is a if statment to cheak the route(link) 
            if the link is == http://basicwebsite.dev/home 
            it will display the showcase file
        --}}
        @if(Request::is('/'))
            {{-- the file thate have the show case code --}}
            @include('inc.showcase')
        @endif
        
        {{-- 
            it will grady the page in to 2 part
            1 part for the content
            2 part for the sidebar    
        --}}
        <div class="row">
            
            <div class="col-lg-9">
                
                {{-- the file that have the error message --}}
                @include('inc.message')
                
                @yield('content')
            
            </div>

            <div class="col-lg-3">
                {{-- it will show in all the pages that have {@extends('layouts.app')} --}}
                @include('inc.sidebar')
            </div>
        </div>
    </div>
    

    {{-- 
        the sidebar will be show in all the pages witch have {{@extends('layouts.app')}}
        and thay all will have it the same place
        
        ## in the home page i have make some new {{@section('sidebar')}}
        ## but it stil show the old {{@section('sidebar')}} white the new
    
        /////////////////

        @section('sidebar')
        <!-- sidebar in all pages -->
            <div>
                <h3> this is a SideBar</h3>
                <p> this sidebar will be show in all pages </p>

                @show

            </div>
    --}}

    {{-- 
        all the code up are in a new file for the sidebar only
        and he is ==== views/inc/sidebar.blade.php 

        and we called like this / (have it in here)
            @include('inc.sidebar')
                 
        #THIS IS THE BEST PRACTICES#
    --}}
    {{-- @include('inc.sidebar') --}}



    <footer class="container border-top p-3 mt-5">
        <p>&copy; Basec Web Site by Laravel 2017-2019</p>
    </footer>


</body>

</html>